import React, { Component } from 'react'
import './Form.css';

export default class Form extends Component {

    constructor() {
        super();
        this.state = {
            city: '',
        }
    }

    onInput = (event) => {
        this.setState({
            city: event.target.value
        })
    }

    

    render() {
        return (
            <form className="form" onSubmit={this.props.weatherData}>
                <div className="form-group">
                    <label htmlFor="city">City Name</label>
                    <input type="text" name="city" id="city" value={this.state.city} onInput={this.onInput} />
                </div>
                <button type="submit" className="submitButton">submit</button>
            </form>
        )
    }
}
