import React, { Component } from 'react'
import axios from 'axios';
import HourlyForeCast from './HourlyForeCast';
import './ForeCastDay.css';
import moment from 'moment';
import config from '../config/config';

export default class ForeCastDay extends Component {


    constructor(props) {
        super(props);
        this.state = {
            date: null,
            country: null,
            population: null,
            hourlyForeCast: null,
            loaded: false,
            degree: "celsius"
        }
    }

    getDay(seconds) {
        const date = new Date(seconds * 1000);
        const dateFormatter = moment(date);
        return dateFormatter.format('ddd');
    }

    getFullDate(seconds) {
        const date = new Date(seconds * 1000);
        const fullDateFormatter = moment(date);
        return fullDateFormatter.format('YYYY-MM-DD');
    }

    handleDegree = (event) => {
        const degree = event.target.value;
        this.setState({
            degree: degree
        })
    }


    async componentDidMount() {

        try {
            const response = await axios.get(`${config.baseUrl}/data/2.5/forecast`, {
                params: {
                    q: this.props.match.params.cityName,
                    units: "metric",
                    appid: config.weatherApiKey
                }
            });

            const hourlyForeCast = response.data.list.filter(data => this.props.match.params.day === this.getDay(data.dt)) ;

            this.setState({
                loaded: true,
                hourlyForeCast: hourlyForeCast,
                country: response.data.city.country,
                population: response.data.city.population,
                date: this.getFullDate(hourlyForeCast[0].dt)
            })

        } catch (e) {
            console.log(e.message);
            this.setState({
                loaded: false,
                hourlyForeCast: null,
            })
        }

    }

    render() {
        return (
            <div className="forecast-day">
                <div className="hourly-title">
                    <p><span>City:</span> {this.props.match.params.cityName}</p>
                    <p><span>Population:</span> {this.state.population}</p>
                    <p><span>Date:</span> {this.state.date}</p>
                    <p><span>Country:</span> {this.state.country || "no data"}</p>
                </div>
                <div className="toggle-degree">
                    <div className="toggle-group">
                        <input type="radio" id="fahrenheit" name="degree" value="fahrenheit" onChange={this.handleDegree} /> 
                        <label htmlFor="fahrenheit">Fahrenheit</label>
                    </div>
                    <div className="toggle-group">
                        <input type="radio" id="celsius" name="degree" checked={this.state.degree === 'celsius'} value="celsius" onChange={this.handleDegree} /> 
                        <label htmlFor="celsius">Celsius</label>
                    </div>
                </div>
                <div className="forecast-day-forecast">
                    {this.state.loaded ?

                        this.state.hourlyForeCast.map(((foreCast, index) => {
                            return <HourlyForeCast
                                key={index}
                                degree={this.state.degree}
                                foreCast={foreCast} />;
                        }))

                        : ""}
                </div>
            </div>
        )
    }
}
