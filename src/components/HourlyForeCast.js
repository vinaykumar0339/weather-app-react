import React from 'react'
import './HourlyForeCast.css';
import moment from 'moment';
import config from '../config/config';

export default function HourlyForeCast(props) {

    function getFullTime(seconds) {
        const time = new Date(seconds * 1000);
        const timeFormatter = moment(time);
        return timeFormatter.format('hh:mm A');
    }

    function celsiusToFahrenheit(celsius) {
        const fahrenheit = parseFloat((celsius * (9 / 5)) + 32);
        return fahrenheit.toFixed(2);
    }

    return (
        <div className="hourly-forecast">
            <p>{getFullTime(props.foreCast.dt)}</p>
            <img src={`${config.baseUrl}/img/w/${props.foreCast.weather[0].icon}.png`} alt={props.foreCast.weather[0].main} />
            <div className="hourly-temperatures">
                {props.degree === 'celsius' ?
                    <p>{props.foreCast.main.temp_max}&deg; C</p> :
                    <p>{celsiusToFahrenheit(props.foreCast.main.temp_max)}&deg; F</p>
                }
                {props.degree === 'celsius' ?
                    <p className="minTemperature">{props.foreCast.main.temp_max}&deg; C</p> :
                    <p className="minTemperature">{celsiusToFahrenheit(props.foreCast.main.temp_max)}&deg; F</p>
                }
            </div>
        </div>
    )

}
