import React from 'react';
import { Link } from 'react-router-dom';
import './Header.css';

export default function Header() {
    return (
        <header className="header">
            <h1>Weather App</h1>
            <Link className="nav-link" to='/' style={{ textDecoration: "none" }}>Home</Link>
        </header>
    );
}

