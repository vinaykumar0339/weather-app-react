import React, { Component } from 'react';
import axios from 'axios';
import config from '../config/config';


import ForeCasts from './forCasts/ForeCasts';
import Form from './Form';

class Home extends Component {


    constructor(props) {
        super(props);
        this.state = {
            city: null,
            foreCasts: null,
            loaded: false,
            error: false,
        }
    }

    getWeatherData = async (event) => {
        try {
            event.preventDefault();
            const cityName = event.target.elements.city.value;
            const response = await axios.get(`${config.baseUrl}/data/2.5/forecast`, {
                params: {
                    q: cityName,
                    units: "metric",
                    appid: config.weatherApiKey
                }
            });

            const dailyWeather = response.data.list.filter(daily => daily.dt_txt.includes('18:00:00'));
            
            this.setState({
                loaded: true,
                error: false,
                foreCasts: dailyWeather,
                city: response.data.city 
            })


        } catch (e) {
            this.setState({
                loaded: false,
                error: true
            })
            console.log(e.message);
        }
    }

    render() {
        return (
            <div className="App">
                <Form weatherData={this.getWeatherData} />
                {this.state.error ? <p className="error">please provide valid city name</p> : ''}
                { this.state.loaded ? <ForeCasts city={this.state.city} foreCasts={this.state.foreCasts} /> : ''}
            </div>
        );
    }
}

export default Home;
