import React from 'react'
import './ForeCastItem.css';
import { Link } from 'react-router-dom';
import moment from 'moment';
import config from '../../config/config';

export default function ForeCastItem(props) {

    function getDay(seconds) {
        const date = new Date(seconds * 1000);
        const dateFormatter = moment(date);
        return dateFormatter.format('ddd');
    }

    function celsiusToFahrenheit(celsius) {
        const fahrenheit = parseFloat((celsius * (9/5)) + 32);
        return fahrenheit.toFixed(2);
    }

    return (
        <Link to={`/${props.cityName}/${getDay(props.foreCast.dt)}`} style={{ textDecoration: "none" }}>
            <div className="forecast">
                <p>{getDay(props.foreCast.dt)}</p>
                <img src={`${config.baseUrl}/img/w/${props.foreCast.weather[0].icon}.png`} alt={props.foreCast.weather[0].main} />
                <div className="temperatures">
                    {props.degree === 'celsius' ?
                        <p>{props.foreCast.main.temp_max}&deg; C</p> :
                        <p>{celsiusToFahrenheit(props.foreCast.main.temp_max)}&deg; F</p>
                    }
                    {props.degree === 'celsius' ?
                        <p className="minTemperature">{props.foreCast.main.temp_max}&deg; C</p> :
                        <p className="minTemperature">{celsiusToFahrenheit(props.foreCast.main.temp_max)}&deg; F</p>
                    }
                </div>
            </div>
        </Link>
    )
}
