import React, { Component } from 'react';
import ForeCastItem from './ForeCastItem';
import './ForeCasts.css';

export default class ForeCasts extends Component {


    constructor(props) {
        super(props);
        this.state = {
            degree: "celsius",
        };
    }

    handleDegree = (event) => {
        const degree = event.target.value;
        this.setState({
            degree: degree
        })
    }

    render() {
        return (
            <div className="forecasts">
                <div className="title">
                    <p><span>City:</span> {this.props.city.name}</p>
                    <p><span>Country:</span> {this.props.city.country || "no data"}</p>
                </div>
                <div className="toggle-degree">
                    <div className="toggle-group">
                        <input type="radio" id="fahrenheit" name="degree" value="fahrenheit" onChange={this.handleDegree} /> 
                        <label htmlFor="fahrenheit">Fahrenheit</label>
                    </div>
                    <div className="toggle-group">
                        <input type="radio" id="celsius" name="degree" checked={this.state.degree === 'celsius'} value="celsius" onChange={this.handleDegree} /> 
                        <label htmlFor="celsius">Celsius</label>
                    </div>
                </div>
                <div className="forecasts-forecast">
                    {this.props.foreCasts.map((foreCast, index) => {
                        return <ForeCastItem 
                                key={index} 
                                degree={this.state.degree} 
                                cityName={this.props.city.name} 
                                foreCast={foreCast} />;
                    })}
                </div>
            </div>
        )
    }
}
