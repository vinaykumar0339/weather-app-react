export default {
    weatherApiKey: process.env.REACT_APP_WEATHER_API_KEY,
    baseUrl: process.env.REACT_APP_BASE_URL
}