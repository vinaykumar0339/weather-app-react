import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import Header from './components/Header';
import ForeCastDay from './components/ForeCastDay';

class App extends Component {

  render() {
    return (
      <Router>
        <div className="App">
          <Header />
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/:cityName/:day" exact component={ForeCastDay} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
